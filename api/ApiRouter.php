<?
$sep = DIRECTORY_SEPARATOR;
$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
$controller = preg_replace('(\W+)', '', $_REQUEST['controller']);
$action = preg_replace('(\W+)', '', $_REQUEST['action']);
$file = ucfirst($controller) . 'Controller';
$controller_file = __DIR__ . $sep . 'controllers' . $sep . $file . '.php';

unset($_REQUEST['controller']);
unset($_REQUEST['action']);

if (file_exists($controller_file)) {
    include_once $controller_file;
} else {
    header($protocol . ' 500 Internal Server Error');
    echo json_encode(
        [
            'success' => false,
            'message' => 'Не найден файл контроллера указанного объекта',
        ]
    );
    exit();
}

if (class_exists($file)) {
    $data = array_filter($_REQUEST);
    $raw = file_get_contents('php://input');
    $reader = new $file($action, $data, $raw);
} else {
    header($protocol . ' 500 Internal Server Error');
    echo json_encode(
        [
            'success' => false,
            'message' => 'Контроллер не существует',
        ]
    );
    exit();
}
