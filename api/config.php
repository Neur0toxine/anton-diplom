<?php
require_once __DIR__ . '/../vendor/autoload.php';

// Загрузка конфигурации
$env = new Dotenv\Dotenv(__DIR__, '/../config.cfg');
$env->load();
$GLOBALS['envcfg'] = $env;

// Определение режима отладки
// if(getenv('DEBUG', false))
// {
//     ini_set('display_errors', On);
//     ini_set('error_reporting', E_ALL);
// }
// else
// {
//     ini_set('display_errors', Off);
//     ini_set('error_reporting', E_ALL & ~E_DEPRECATED & ~E_STRICT);
// }

// Подключение к БД и настройка ORM.
$pdo = new \PDO("mysql:dbname=" . getenv('DB_NAME', 'database') . ";host=" . getenv('DB_HOST', '127.0.0.1'), getenv('DB_USER', 'user'), getenv('DB_PASSWORD', 'password'));
$GLOBALS['pdo'] = $pdo;
$GLOBALS['dbh'] = new \LessQL\Database($pdo);
$GLOBALS['dbh']->setReference('reports', 'users', 'id');
$GLOBALS['dbh']->setReference('reports', 'equipment', 'id');
