<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../ApiConstruct.php';
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
//use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;

class ReportController extends ApiConstruct
{
    protected $routes = [
        'register', 'read', 'readOne', 'update', 'delete', 'report'
    ];
    protected $authOnlyRoutes = [
        'register' => 0,
        'read' => 0,
        'readOne' => 0,
        'update' => 1,
        'delete' => 1
    ];

    public function register()
    {
        $this->required(
            [
                'equipment_id' => 'Укажите ID техники',
                'theme' => 'Укажите тему заявки',
                'cabinet' => 'Укажите № кабинета',
                'problem' => 'Укажите описание проблемы',
            ]
        );
        $user = $this->Auth->getUser();
        $tech = $this->db->equipment()->where('id', intval($this->var('equipment_id')))->fetch();
        if (!$tech) {
            $this->send(['message' => 'Техника не найдена'], false);
            exit();
        }
        $row = $this->db->createRow(
            'reports',
            [
                'equipment_id' => intval($this->var('equipment_id')),
                'user_id' => intval($user['id']),
                'theme' => $this->var('theme'),
                'cabinet' => intval($this->var('cabinet')),
                'problem' => $this->var('problem'),
                'solution' => '',
                'status' => 0,
            ]
        );

        $success = false;

        try {
            $subject = htmlspecialchars($this->var('theme') . ', каб. ' . $this->var('cabinet'));
            $message = '
            <html>
            <head>
            <title>' . $subject . '</title>
            <style>
            table {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            table tr:nth-child(even){background-color: #f2f2f2;}

            table tr:hover {background-color: #ddd;}

            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                width: 120px;
                text-align: left;
                background-color: #c0c0c0;
                color: black;
            }
            </style>
            </head>
            <body>
            <table>
                <tr>
                <th><b>Тема заявки</b></th>
                <td>' . htmlspecialchars($this->var('theme')) . '</td>
                </tr>
                <tr>
                <th><b>№ кабинета</b></th>
                <td>' . htmlspecialchars($this->var('cabinet')) . '</td>
                </tr>
                <tr>
                <th><b>Техника</b></th>
                <td>' . htmlspecialchars($tech['name']) . ', ID: ' . htmlspecialchars($tech['id']) . '</td>
                </tr>
                <tr>
                <th><b>Описание проблемы</b></th>
                <td>' . nl2br(htmlspecialchars($this->var('problem'))) . '</td>
                </tr>
            </table>
            </body>
            </html>
            ';

            $plain_message = '
            Тема заявки: ' . htmlspecialchars($this->var('theme')) . '
            Кабинет: ' . htmlspecialchars($this->var('cabinet')) . '
            Техника: ' . htmlspecialchars($tech['name']) . ', ID: ' . htmlspecialchars($tech['id']) . '
            Описание проблемы: ' . htmlspecialchars($this->var('problem'));

            $mail = new PHPMailer(true);
            $mail->SMTPAutoTLS = true;
            $mail->isSMTP();
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                ),
            );
            $mail->CharSet = getenv('EMAIL_CHARSET', 'UTF-8');
            $mail->Host = getenv('EMAIL_SMTP_HOST', 'localhost');
            $mail->SMTPAuth = getenv('EMAIL_SMTP_AUTH', true);
            $mail->Username = getenv('EMAIL_SMTP_USERNAME', 'user');
            $mail->Password = getenv('EMAIL_SMTP_PASSWORD', 'password');
            $mail->SMTPSecure = getenv('EMAIL_SMTP_SECURE', 'tls');
            $mail->Port = getenv('EMAIL_SMTP_PORT', 443);

            $mail->setFrom(getenv('EMAIL_FROM', 'noreply@localhost'), 'Уведомление о заявке');
            $mail->addAddress(getenv('EMAIL_RECIPIENT', 'admin@localhost'));

            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body = $message;
            $mail->AltBody = $plain_message;

            $mail->send();
        } catch (Exception $e) {
            $this->send(['message' => 'Не удалось отправить уведомление о заявке'], false);
            exit();
        }

        try {
            $this->db->begin();
            $row->save();
            $this->db->commit();

            $success = true;
        } catch (Exception $e) {
            $this->db->rollback();
            $this->send(['message' => 'Не удалось зарегистрировать заявку'], false);
            exit();
        }

        $this->send([], true);
    }

    public function read()
    {
        $this->required(
            [
                'page' => 'Укажите № страницы',
                'limit' => 'Укажите лимит объектов на страницу (не более 50)',
            ]
        );
        $user = $this->Auth->getUser();
        $page = intval($this->var('page'));
        $limit = (intval($this->var('limit')) <= 50 ? intval($this->var('limit')) : 50);
        $request = null;
        if(in_array($user['role'], [0, 1])) {
            $request = $this->db->reports()->where('user_id', $user['id'])->orderBy('created_at', 'DESC')->orderBy('status');
        } else {
            $request = $this->db->reports()->orderBy('created_at', 'DESC')->orderBy('status');
        }
        $total = $request->count();
        $totalPages = ceil($total / $limit);
        $result = $request->paged($limit, $page);
        $final_result = [];
        foreach ($result as $res) {
            $equipment = $result->equipment()->via('equipment_id')->fetch();
            $item = $res;
            $item['equipment'] = $equipment;
            array_push($final_result, $item);
            unset($equipment);
        }
        $this->send(
            [
                'page' => $page,
                'totalPages' => $totalPages,
                'totalObjects' => $total,
                'docs' => $final_result
            ]
        , true);
    }

    public function report() {
        // Получение дат, по которым будет строиться отчёт
        $from_in = $this->var('from');
        $to_in = $this->var('to');
        $from = date_create_from_format('d.m.Y', $from_in);
        $to = date_create_from_format('d.m.Y', $to_in);
        $from_db = $this->db->quote(date('Y-m-d G:i:s', date_format($from, 'U')));
        $to_db = $this->db->quote(date('Y-m-d G:i:s', date_format($to, 'U')));

        // Получаем данные из БД
        $results = [];
        $raw_db = $this->db->query('SELECT `reports`.*, `users`.`id` AS `this_user_id`, `users`.`firstName`, `users`.`lastName`, `equipment`.`id` AS `this_equipment_id`, `equipment`.`name` FROM `reports`, `users`, `equipment` WHERE `reports`.`user_id`=`users`.`id` AND `reports`.`equipment_id`=`equipment`.`id`;')->fetchAll(PDO::FETCH_ASSOC);
        $results = array_map(function($res){
            return [
                'id' => $res['id'],
                'user' => [
                    'id' => $res['this_user_id'],
                    'firstName' => $res['firstName'],
                    'lastName' => $res['lastName']
                ],
                'equipment' => [
                    'id' => $res['this_equipment_id'],
                    'name' => $res['name']
                ],
                'theme' => $res['theme'],
                'cabinet' => $res['cabinet'],
                'problem' => $res['problem'],
                'solution' => $res['solution'],
                'status' => $res['status'],
                'created_at' => $res['created_at']
            ];
        }, $raw_db);
        unset($raw_db);

        // Установка стандартного шрифта всего отчёта
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getDefaultStyle()->getFont()->setName('Segoe UI');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(12);

        // Устанавливаем ширину и высоту строк и столбцов в заголовке
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getColumnDimension('A')->setWidth(4);
        $sheet->getColumnDimension('B')->setWidth(22);
        $sheet->getRowDimension('1')->setRowHeight(16);
        $sheet->getRowDimension('2')->setRowHeight(25);

        // Устанавливаем заголовок
        $sheet->setCellValue('B2', 'Отчёт по выполненным ремонтам с ' . date_format($from, 'd.m.Y') . ' по ' . date_format($to, 'd.m.Y'));

        // Группируем ячейки заголовка
        $sheet->mergeCells('B2:K2');

        // Устанавливаем стили заголовка
        $headerStyle = [
            'font' => [
                'bold' => true,
                'size' => 14
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => ['rgb' => '000000']
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => ['rgb' => 'E5E4E2']
            ],
        ];
        $sheet->getStyle('B2:K2')->applyFromArray($headerStyle);

        $runner = 4;
        $item_header = function(&$sheet, &$r, &$style, $res, $simplestr = false) {
            if($simplestr) $sheet->setCellValue('B' . $r, $res);
            else {
                $date = date_create_from_format('Y-m-d H:i:s', $res['created_at']);
                $sheet->setCellValue('B' . $r, 'Заявка от ' . date_format($date, 'H:i:s d.m.Y'));
            }
            $sheet->mergeCells('B' . $r . ':K' . $r);
            $sheet->getStyle('B' . $r . ':K' . $r)->applyFromArray($style);
            //$sheet->mergeCells('B' . ($r + 1) . ':K' . ($r + 1));
            $r += 1;
        };
        $keyvalue = function(&$sheet, &$r, $key, $value, $multistring = false) {
            $leftHeaderStyle = [
                'font' => [
                    'bold' => true,
                    'size' => 11
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['rgb' => '7d7d7d']
                    ],
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => ['rgb' => 'd4d3d3']
                ],
            ];
            $itemContentsStyle = [
                'font' => [
                    'bold' => false,
                    'size' => 11
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['rgb' => '7d7d7d']
                    ],
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => ['rgb' => 'FFFFFF']
                ],
            ];
            $sheet->setCellValue('B' . $r, $key);
            $sheet->setCellValue('C' . $r, $value);
            $sheet->mergeCells('C' . $r . ':K' . $r);
            $sheet->getStyle('C' . $r . ':K' . $r)->applyFromArray($itemContentsStyle);
            if($multistring) {
                $sheet->getStyle('C' . $r . ':K' . $r)->getAlignment()->setWrapText(true);
                $sheet->getRowDimension($r)->setRowHeight(110);
            }
            $sheet->getStyle('B' . $r)->applyFromArray($leftHeaderStyle);
            //$sheet->mergeCells('B' . ($r + 1) . ':K' . ($r + 1));
            $r += 1;
        };
        function printItem($item_header, $keyvalue, &$sheet, &$r, $res) {
            $itemHeaderStyle = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUMDASHED,
                        'color' => ['rgb' => '828282']
                    ],
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => ['rgb' => 'E5E4E2']
                ],
            ];
            $status = '';
            switch($res['status']) {
                case 0:
                    $status = 'Заявка открыта';
                    break;
                case 1:
                    $status = 'В процессе решения';
                    break;
                case 2:
                    $status = 'Закрыта автором';
                    break;
                case 3:
                    $status = 'Проблема решена';
            }
            $item_header($sheet, $r, $itemHeaderStyle, $res);
            $keyvalue($sheet, $r, 'Тема заявки', $res['theme']);
            $keyvalue($sheet, $r, 'Кем создан', $res['user']['firstName'] . ' ' . $res['user']['lastName']);
            $keyvalue($sheet, $r, 'Кабинет', $res['cabinet']);
            $keyvalue($sheet, $r, 'Объект ремонта', $res['equipment']['name'] . ', ID: ' . $res['equipment']['id']);
            $keyvalue($sheet, $r, 'Проблема', $res['problem'], true);
            $keyvalue($sheet, $r, 'Решение', strlen($res['solution']) > 0 ? $res['solution'] : 'Нет решения', true);
            $keyvalue($sheet, $r, 'Статус', $status);
            $r += 1;
        };
    
        $opened = 0;
        $inprogress = 0;
        $closed = 0;
        $solved = 0;
        foreach($results as $res) {
            switch($res['status']) {
                case 0:
                    $opened += 1;
                    break;
                case 1:
                    $inprogress += 1;
                    break;
                case 2:
                    $closed += 1;
                    break;
                case 3:
                    $solved += 1;
            }
            printItem($item_header, $keyvalue, $sheet, $runner, $res);
        }

        $runner += 2;
        $itemHeaderStyle = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                        'color' => ['rgb' => '222222']
                    ],
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => ['rgb' => 'FFFFFF']
                ],
            ];
        $item_header($sheet, $runner, $itemHeaderStyle, 'Итого', true);
        $keyvalue($sheet, $runner, 'Открытых заявок', $opened);
        $keyvalue($sheet, $runner, 'Заявок в обработке', $inprogress);
        $keyvalue($sheet, $runner, 'Закрытых заявок', $closed);
        $keyvalue($sheet, $runner, 'Решённых заявок', $solved);
        $keyvalue($sheet, $runner, 'Нерешённых заявок', ($opened + $inprogress + $closed));

        $spreadsheet->getProperties()
        ->setCreator("Генератор отчётов по ремонту техники")
        ->setLastModifiedBy("Генератор отчётов по ремонту техники")
        ->setTitle("Отчёт по ремонту компьютернй техники в школе")
        ->setSubject("Отчёт по ремонту компьютернй техники в школе")
        ->setDescription(
            "Отчёт по ремонту компьютернй техники в школе"
        )
        ->setKeywords("ремонт компьютер техника")
        ->setCategory("Отчёт");

        // Отправляем HTTP-заголовки, мимикрируя под файл документа.
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="report-' . date_format($from, 'd.m.Y') . '-' . date_format($to, 'd.m.Y') . '.xlsx"');
        //header('Content-Disposition: attachment;filename="Report.xls"');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        // Генерируем экселевский файл, отправляем его клиенту.
        $writer = new Xlsx($spreadsheet);
        //$writer = new Xls($spreadsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save('php://output');
    }

    public function readOne()
    {
        $this->required(['id' => 'Укажите ID объекта']);
        $row = $this->db->reports()->where('id', intval($this->var('id')))->fetch();
        $this->send(['object' => $row], true);
    }

    public function update()
    {
        $this->required(['id' => 'Укажите ID объекта']);
        $id = intval($this->var('id'));
        $row = $this->db->reports()->where('id', $id)->fetch();
        $data = $this->request->data;
        unset($data['id']);
        if ($row) {
            try {
                $this->db->begin();
                $row->update($data);
                $this->db->commit();
                $this->send([], true);
            } catch (Exception $e) {
                $this->db->rollback();
                $this->send(['message' => 'Не удалось обновить данные'], false);
            }
        } else {
            $this->send(['message' => 'Объект не найден'], false);
        }
    }

    public function delete()
    {
        $this->required(['id' => 'Укажите ID объекта']);
        $id = intval($this->var('id'));
        $row = $this->db->reports()->where('id', $id)->fetch();
        if (!$row) {
            $this->send([], true);
            exit();
        }
        try {
            $this->db->begin();
            $row->delete();
            $this->db->commit();
            $this->send([], true);
        } catch (Exception $e) {
            $this->db->rollback();
            $this->send([], false);
        }
    }
}
