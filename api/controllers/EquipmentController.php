<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../ApiConstruct.php';

class EquipmentController extends ApiConstruct
{
    protected $routes = [
        'create', 'read', 'readOne', 'autocomplete', 'update', 'delete', 'roomAutocomplete'
    ];
    protected $authOnlyRoutes = [
        'create' => 1,
        'read' => 1,
        'readOne' => 1,
        'autocomplete' => 0,
        'update' => 1,
        'delete' => 1,
    ];

    public function create()
    {
        $this->required(
            [
                'room' => 'Укажите номер класса',
                'name' => 'Укажите название единицы техники',
            ]
        );
        $row = $this->db->createRow(
            'equipment',
            [
                'room' => intval($this->var('room')),
                'name' => $this->var('name'),
            ]
        );

        $success = false;
        try {
            $this->db->begin();
            $row->save();
            $this->db->commit();
            $success = true;
        } catch (Exception $e) {
            $this->db->rollback();
            $success = false;
        }

        $this->send([], $success);
    }

    public function read()
    {
        $this->required(
            [
                'page' => 'Укажите № страницы',
                'limit' => 'Укажите лимит объектов на страницу (не более 50)',
            ]
        );
        $page = intval($this->var('page'));
        $limit = (intval($this->var('limit')) <= 50 ? intval($this->var('limit')) : 50);
        $total = $this->db->equipment()->orderBy('room', 'ASC')->count();
        $totalPages = ceil($total / $limit);
        $result = $this->db->equipment()->orderBy('room', 'ASC')->paged($limit, $page);
        $this->send(
            [
                'page' => $page,
                'totalPages' => $totalPages,
                'totalObjects' => $total,
                'docs' => $result
            ]
        , true);
    }

    public function readOne()
    {
        $this->required(['id' => 'Укажите ID объекта']);
        $row = $this->db->equipment()->where('id', intval($this->var('id')))->fetch();
        $this->send(['object' => $row], true);
    }

    public function autocomplete()
    {
        $this->required(['query' => 'Необходимо указать запрос']);
        $rows = null;
        $room = intval($this->var('room'));
        $query = $GLOBALS['pdo']->quote('%' . str_replace('%', '\\%', $this->var('query')) . '%');
        if (empty($room)) {
            $rows = $this->db->query("SELECT * FROM `equipment` WHERE `name` LIKE " . $query . " COLLATE utf8_general_ci")->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $rows = $this->db->query("SELECT * FROM `equipment` WHERE `room`=" . $room . " AND `name` LIKE " . $query . " COLLATE utf8_general_ci")->fetchAll(PDO::FETCH_ASSOC);

        }
        $this->send(['docs' => $rows], true);
    }

    public function roomAutocomplete() {
        $this->required(['query' => 'Необходимо указать запрос']);
        $room = intval($this->var('query'));
        if(!empty($room)) {
            $query = $GLOBALS['pdo']->quote('%' . str_replace('%', '\\%', $room) . '%');
            $sql = "SELECT `room` FROM `equipment` WHERE `room` LIKE " . $query . " GROUP BY `room` ORDER BY `room`";
            $rows = $this->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            $rooms = array_map(function($val){ return $val['room']; }, $rows);
            $this->send(['docs' => $rooms], true);
        } else {
            $this->send(['message' => 'В запросе должны быть только числа'], false);
        }
    }

    public function update()
    {
        $this->required(['id' => 'Укажите ID объекта']);
        $id = intval($this->var('id'));
        $row = $this->db->equipment()->where('id', $id)->fetch();
        $data = $this->request->data;
        unset($data['id']);
        if ($row) {
            try {
                $this->db->begin();
                $row->update($data);
                $this->db->commit();
                $this->send([], true);
            } catch (Exception $e) {
                $this->db->rollback();
                $this->send(['message' => 'Не удалось обновить данные'], false);
            }
        } else {
            $this->send(['message' => 'Объект не найден'], false);
        }
    }

    public function delete()
    {
        $this->required(['id' => 'Укажите ID объекта']);
        $id = intval($this->var('id'));
        $row = $this->db->equipment()->where('id', $id)->fetch();
        if (!$row) {
            $this->send([], true);
            exit();
        }
        try {
            $this->db->begin();
            $row->delete();
            $this->db->commit();
            $this->send([], true);
        } catch (Exception $e) {
            $this->db->rollback();
            $this->send([], false);
        }
    }
}
