<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../ApiConstruct.php';
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\ValidationData;

class AuthController extends ApiConstruct
{
    protected $routes = [
        'register',
        'login',
        'me',
        'update',
        'delete',
        'read'
    ];

    protected $authOnlyRoutes = ['update' => 2, 'delete' => 2, 'read' => 2];

    public function login()
    {
        $this->required([
            'login' => 'Укажите имя пользователя',
            'password' => 'Укажите пароль',
        ]);
        $row = $this->db->table('users')->where('nickname', $this->var('login'))->fetch();
        if (empty($row)) {
            return $this->send([
                'message' => 'Пользователь не найден',
            ], false);
        } else if (password_verify($this->var('password'), $row['password'])) {
            try {
                $token = $this->generate_token(
                    $row['id'],
                    $row['firstName'],
                    $row['lastName'],
                    $row['nickname'],
                    $row['role']
                );
                return $this->send([
                    'token' => (string) $token,
                ], true);
            } catch (Exception $e) {
                return $this->send([], false);
            }
        } else {
            return $this->send([
                'message' => 'Неверный пароль',
            ], false);
        }

    }
    public function register()
    {
        $this->required([
            'firstName' => 'Укажите Ваше имя',
            'lastName' => 'Укажите Вашу фамилию',
            'login' => 'Укажите имя пользователя',
            'password' => 'Укажите Ваш пароль',
        ]);
        $password = password_hash($this->var('password'), PASSWORD_DEFAULT);
        $row = $this->db->createRow('users', [
            'firstName' => $this->var('firstName'),
            'lastName' => $this->var('lastName'),
            'nickname' => $this->var('login'),
            'password' => $password,
            'role' => 0,
        ]);
        $success = null;
        try {
            $this->db->begin();
            $row->save();
            $this->db->commit();
            $success = true;
        } catch (Exception $e) {
            $this->db->rollback();
            $success = false;
        }
        return $this->send([], true);
    }
    public function generate_token($id, $firstName, $lastName, $login, $role)
    {
        $signer = new Sha256();
        return (new Builder())
            ->setId(getenv('JWT_SIGN_ID', 'JWT_TEST_ID'), true)
            ->setIssuedAt(time())
            ->setNotBefore(time() + 60)
            ->setExpiration(time() + 31557600)
            ->set('id', $id)
            ->set('firstName', $firstName)
            ->set('lastName', $lastName)
            ->set('login', $login)
            ->set('role', $role)
            ->sign($signer, getenv('JWT_SIGN_KEY', 'JWT_TEST_KEY'))
            ->getToken();
    }

    public function me()
    {
        $this->required([
            'token' => 'Для проверки необходим токен аутентификации',
        ]);
        if ($this->verify_token($this->var('token'))) {
            return $this->send($this->token_data($this->var('token')), true);
        } else {
            return $this->send([], false);
        }

    }

    public function isAuthorized()
    {
        $token = $this->getToken();
        if (empty($token)) {
            return false;
        } else {
            try {
                return $this->verify_token($token);
            } catch (Exception $e) {
                return false;
            }
        }
    }

    public function update()
    {
        $this->required(['id' => 'Укажите ID объекта']);
        $id = intval($this->var('id'));
        $row = $this->db->users()->where('id', $id)->fetch();
        $data = $this->request->data;
        if(isset($data['password']) || !empty($data['password'])) {
            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        }
        unset($data['id']);
        if ($row) {
            try {
                $this->db->begin();
                $row->update($data);
                $this->db->commit();
                $this->send([], true);
            } catch (Exception $e) {
                $this->db->rollback();
                $this->send(['message' => 'Не удалось обновить данные'], false);
            }
        } else {
            $this->send(['message' => 'Объект не найден'], false);
        }
    }

    public function read()
    {
        $this->required(
            [
                'page' => 'Укажите № страницы',
                'limit' => 'Укажите лимит объектов на страницу (не более 50)',
            ]
        );
        $page = intval($this->var('page'));
        $limit = (intval($this->var('limit')) <= 50 ? intval($this->var('limit')) : 50);
        $request = $this->db->users()->orderBy('firstName', 'ASC');
        $total = $request->count();
        $totalPages = ceil($total / $limit);
        $result = $request->paged($limit, $page);
        $this->send(
            [
                'page' => $page,
                'totalPages' => $totalPages,
                'totalObjects' => $total,
                'docs' => $result
            ]
        , true);
    }

    public function delete()
    {
        $this->required(['id' => 'Укажите ID объекта']);
        $id = intval($this->var('id'));
        $row = $this->db->users()->where('id', $id)->fetch();
        if (!$row) {
            $this->send([], true);
            exit();
        }
        try {
            $this->db->begin();
            $row->delete();
            $this->db->commit();
            $this->send([], true);
        } catch (Exception $e) {
            $this->db->rollback();
            $this->send([], false);
        }
    }

    public function getToken()
    {
        $headers = apache_request_headers();
        $auth_header = isset($headers['Authorization']) ? $headers['Authorization'] : (isset($headers['authorization']) ? $headers['authorization'] : '');
        return !empty($auth_header) ? preg_replace("(Bearer\s+?)", '', $auth_header) : '';
    }

    public function getUser() {
        $token = $this->getToken();
        if(!empty($token)) {
            return $this->token_data($token);
        } else return [];
    }

    public function verify_token($token)
    {
        $signer = new Sha256();
        $data = new ValidationData();
        $data->setId(getenv('JWT_SIGN_ID', 'JWT_TEST_ID'));
        if (gettype($token) == 'string') {
            $token = (new Parser())->parse((string) $token);
        }
        return $token->verify($signer, getenv('JWT_SIGN_KEY', 'JWT_TEST_KEY'));
    }
    public function token_data($token)
    {
        if (gettype($token) == 'string') {
            $token = (new Parser())->parse((string) $token);
        };
        // if ($token == null) {
        //     return [];
        // }
        return [
            'id' => $token->getClaim('id'),
            'firstName' => $token->getClaim('firstName'),
            'lastName' => $token->getClaim('lastName'),
            'login' => $token->getClaim('login'),
            'role' => $token->getClaim('role'),
        ];
    }
}
