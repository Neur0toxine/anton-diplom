<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
class RequestBody
{
    public function __construct($form, $raw)
    {
        $this->raw = $raw;
        $this->data = [];
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0';
        try {
            $this->data = json_decode($raw, true);
        } catch (Exception $e) {
            echo json_encode([
                'success' => false,
                'message' => 'Incorrect JSON provided',
            ]);
            exit();
        }
    }

    public function is($method)
    {
        return $this->method == $method;
    }

    public function send($arr, $http_code = 200)
    {
        $this->http_response_code(intval($http_code));
        header('Content-Type: application/json');
        echo json_encode($arr);
    }

    public function http_response_code($code = null)
    {
        if ($code !== null) {
            switch ($code) {
                case 100:$text = 'Continue';
                    break;
                case 101:$text = 'Switching Protocols';
                    break;
                case 200:$text = 'OK';
                    break;
                case 201:$text = 'Created';
                    break;
                case 202:$text = 'Accepted';
                    break;
                case 203:$text = 'Non-Authoritative Information';
                    break;
                case 204:$text = 'No Content';
                    break;
                case 205:$text = 'Reset Content';
                    break;
                case 206:$text = 'Partial Content';
                    break;
                case 300:$text = 'Multiple Choices';
                    break;
                case 301:$text = 'Moved Permanently';
                    break;
                case 302:$text = 'Moved Temporarily';
                    break;
                case 303:$text = 'See Other';
                    break;
                case 304:$text = 'Not Modified';
                    break;
                case 305:$text = 'Use Proxy';
                    break;
                case 400:$text = 'Bad Request';
                    break;
                case 401:$text = 'Unauthorized';
                    break;
                case 402:$text = 'Payment Required';
                    break;
                case 403:$text = 'Forbidden';
                    break;
                case 404:$text = 'Not Found';
                    break;
                case 405:$text = 'Method Not Allowed';
                    break;
                case 406:$text = 'Not Acceptable';
                    break;
                case 407:$text = 'Proxy Authentication Required';
                    break;
                case 408:$text = 'Request Time-out';
                    break;
                case 409:$text = 'Conflict';
                    break;
                case 410:$text = 'Gone';
                    break;
                case 411:$text = 'Length Required';
                    break;
                case 412:$text = 'Precondition Failed';
                    break;
                case 413:$text = 'Request Entity Too Large';
                    break;
                case 414:$text = 'Request-URI Too Large';
                    break;
                case 415:$text = 'Unsupported Media Type';
                    break;
                case 500:$text = 'Internal Server Error';
                    break;
                case 501:$text = 'Not Implemented';
                    break;
                case 502:$text = 'Bad Gateway';
                    break;
                case 503:$text = 'Service Unavailable';
                    break;
                case 504:$text = 'Gateway Time-out';
                    break;
                case 505:$text = 'HTTP Version not supported';
                    break;
                default:
                    return false;
                    break;
            }
            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
            header($protocol . ' ' . $code . ' ' . $text);
            $GLOBALS['http_response_code'] = $code;
        } else {
            $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);
        }
        return $code;
    }

    function var ($var) {
        if (isset($this->data[$var])) {
            return $this->data[$var];
        } else {
            return null;
        }

    }
}

class ApiConstruct
{
    public function __construct($action, $data, $raw)
    {
        $this->request = new RequestBody($data, $raw);
        $this->db = $GLOBALS['dbh'];
        $this->pdo = $GLOBALS['pdo'];
        $this->config = $GLOBALS['envcfg'];
        if ($action != 'CHECK_AUTH') {
            if (!empty($this->authOnlyRoutes)) {
                $sep = DIRECTORY_SEPARATOR;
                include_once __DIR__ . $sep . 'controllers' . $sep . 'AuthController.php';
                $this->Auth = new AuthController('CHECK_AUTH', $data, $raw);
            }
            if (in_array($action, $this->routes)) {
                if (isset($this->authOnlyRoutes[$action])) {
                    if ($this->Auth->isAuthorized()) {
                        $token = $this->Auth->getToken();
                        $role = intval($this->Auth->token_data($token)['role']);
                        if ($role >= intval($this->authOnlyRoutes[$action])) {
                            $this->$action();
                        } else {
                            $this->send(
                                [
                                    'message' => 'Вашего уровня доступа недостаточно',
                                ],
                                false, 401);
                            exit();
                        }
                    } else {
                        $this->send(
                            [
                                'message' => 'Этот путь доступен только зарегистрированным пользователям',
                            ],
                            false, 401);
                        exit();
                    }
                } else {
                    $this->$action();
                }
            } else {
                $this->send(['message' => 'Указанное действие не обнаружено'], false, 500);
                exit();
            }
        }
        /*
    if ($action != null && $data != null && $raw != null && $authChecker != null) {
    $this->request = new RequestBody($data, $raw);
    $this->db = $GLOBALS['dbh'];
    $this->config = $GLOBALS['envcfg'];
    if (isset($this->protectedRoutes)) {
    if (in_array($action, $this->protectedRoutes)) {
    if ($authChecker($data)) {
    $this->$action();
    } else {
    $this->send([
    message => 'Требуется авторизация',
    ], 401);
    exit();
    }
    } else {
    $this->$action();
    }

    }
    $this->$action();
    }
     */
    }
    public function __destruct()
    {
        unset($this->request);
    }
    public function required($array_of_vars, $request_type = '')
    {
        $type = empty($request_type) ? $request_type : $this->request->type;
        foreach ($array_of_vars as $k => $v) {
            if (!isset($this->request->data[$k]) || (empty($this->request->data[$k]) && $this->request->data[$k] != 0)) {
                header($this->request->protocol . ' 500 Internal Server Error');
                $this->request->send([
                    'success' => false,
                    'message' => $v,
                ]);
                exit();
            }
        }
    }
    function var ($variable) {
        return $this->request->data[$variable];
    }
    public function send($arr, $success = false, $http_code = 200)
    {
        $arr['success'] = isset($success) ? $success : false;
        return $this->request->send($arr, $http_code);
    }

    public function join_paths(...$paths) {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }

    public function create()
    {}
    public function read()
    {}
    public function update()
    {}
    public function delete()
    {}
}
