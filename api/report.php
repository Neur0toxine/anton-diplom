<?php
$from = $_GET['from'];
$to = $_GET['to'];
$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
if(!isset($from) || empty($from) || !isset($to) || empty($to)) {
    header($protocol . ' 400 Bad Request');
    exit();
}

include_once __DIR__ . '/controllers/ReportController.php';
$raw = json_encode(
    [
        'from' => $from,
        'to' => $to
    ]
);
$controller = new ReportController('report', array_filter($_REQUEST), $raw);
?>