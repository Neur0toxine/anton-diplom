/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'
import LoginPage from '@/components/LoginPage'
import RegisterPage from '@/components/RegisterPage'
import UserPage from '@/components/UserPage'
import AdminPage from '@/components/AdminPage'
import ReportsList from '@/components/UserPage/ReportsList'
import RegisterReport from '@/components/UserPage/RegisterReport'
import ReportGenerator from '@/components/UserPage/ReportGenerator'
import AdminReportsList from '@/components/AdminPage/AdminReportsList'
import UsersList from '@/components/AdminPage/UsersList'
import EquipmentList from '@/components/AdminPage/EquipmentList'
import AdminEquipmentAdd from '@/components/AdminPage/AdminEquipmentAdd'

Vue.use(Router)

var router = new Router({
  routes: [{
      path: '/',
      name: 'LoginPage',
      component: LoginPage,
      meta: {
        title: 'Вход в БД'
      }
    },
    {
      path: '/register',
      name: 'RegisterPage',
      component: RegisterPage,
      meta: {
        title: 'Регистрация'
      }
    },
    {
      path: '/main',
      name: 'UserPage',
      component: UserPage,
      meta: {
        title: 'Личный кабинет'
      },
      children: [{
          path: '',
          redirect: 'reports'
        },
        {
          path: 'reports',
          name: 'ReportsList',
          component: ReportsList,
          meta: {
            title: 'Список заявок - ЛК'
          }
        },
        {
          path: 'registerReport',
          name: 'RegisterReport',
          component: RegisterReport,
          meta: {
            title: 'Регистрация заявки - ЛК'
          }
        },
        {
          path: 'rept',
          name: 'ReportGenerator',
          component: ReportGenerator,
          meta: {
            title: 'Отчёт - ЛК'
          }
        }
      ]
    },
    {
      path: '/admin',
      name: 'AdminPage',
      component: AdminPage,
      meta: {
        title: 'Панель администратора'
      },
      children: [{
          path: '',
          redirect: 'reports'
        },
        {
          path: 'reports',
          name: 'AdminReportsList',
          component: AdminReportsList,
          meta: {
            title: 'Список заявок - Админпанель'
          }
        },
        {
          path: 'users',
          name: 'UsersList',
          component: UsersList,
          meta: {
            title: 'Список пользователей - Админпанель'
          }
        },
        {
          path: 'equipment',
          name: 'AdminEquipmentList',
          component: EquipmentList,
          meta: {
            title: 'Список пользователей - Админпанель'
          }
        },
        {
          path: 'equipmentAdd',
          name: 'AdminEquipmentAdd',
          component: AdminEquipmentAdd,
          meta: {
            title: 'Добавление техники - Админпанель'
          }
        },
        {
          path: 'rept',
          name: 'AdminReportGenerator',
          component: ReportGenerator,
          meta: {
            title: 'Отчёт - ЛК'
          }
        }
      ]
    }
  ]
})

// This callback runs before every route change, including on page load.
router.beforeEach((to, from, next) => {
  // This goes through the matched routes from last to first, finding the closest route with a title.
  // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title)

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags)
  const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags)

  // If a route with a title was found, set the document (page) title to that value.
  if (nearestWithTitle) document.title = nearestWithTitle.meta.title

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el))

  // Skip rendering meta tags if there are none.
  if (!nearestWithMeta) return next()

  // Turn the meta tag definitions into actual elements in the head.
  nearestWithMeta.meta.metaTags.map(tagDef => {
      const tag = document.createElement('meta')

      Object.keys(tagDef).forEach(key => {
        tag.setAttribute(key, tagDef[key])
      })

      // We use this to track which meta tags we create, so we don't interfere with other ones.
      tag.setAttribute('data-vue-router-controlled', '')

      return tag
    })
    // Add the meta tags to the document head.
    .forEach(tag => document.head.appendChild(tag))

  next()
})

export default router
