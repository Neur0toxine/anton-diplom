// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import axios from 'axios'
import SuiVue from 'semantic-ui-vue'
import 'semantic-ui-css/semantic.min.css'
import 'animate.css/animate.min.css'
import App from './App'
import router from './router'

Vue.config.productionTip = false
Vue.use(SuiVue)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  data: {
    token: localStorage.getItem('auth') || '',
    headers: {
      Authorization: `Bearer ${localStorage.getItem('auth')}`
    },
    userData: JSON.parse(localStorage.getItem('user') || "{}")
  },
  router,
  components: {
    App
  },
  methods: {
    me(token) {
      return new Promise((resolve, reject) => {
        var sToken = token || localStorage.getItem("auth") || this.$root.token;
        axios
          .post("/api/auth/me", {
            token: sToken
          })
          .then(res => {
            if (res.data.success || false) {
              let userData = {
                firstName: res.data.firstName,
                lastName: res.data.lastName,
                login: res.data.login,
                role: +res.data.role
              };
              localStorage.setItem('auth', sToken);
              localStorage.setItem('user', JSON.stringify(userData));
              this.userData = userData;
              this.token = sToken;
              this.headers = {
                Authorization: `Bearer ${sToken}`
              };
              resolve({ ...userData,
                token: sToken
              });
            } else {
              this.userData = {};
              this.token = '';
              localStorage.removeItem("auth");
              localStorage.removeItem('user');
              reject(false);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    logout() {
      localStorage.removeItem("auth");
      localStorage.removeItem("user");
      this.token = "";
      this.userData = {};
      this.headers = {};
      this.$router.push({ path: "/" });
    }
  },
  template: '<App/>'
})
